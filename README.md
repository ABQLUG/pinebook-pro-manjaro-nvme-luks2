# Installing Manjaro ARM on PBPro with LUKS2 encryption #

This is an interactive script to help bootstrap Manjaro ARM onto an NVMe disk with LUKS2 encryption.

At the time of writing, there is no option to use LUKS encryption. However, that might change with someday. It would be best to check [this issue tracker](https://gitlab.manjaro.org/manjaro-arm/applications/manjaro-arm-installer/-/issues/22).

However, if you manually install Manjaro ARM. You can add LUKS encryption manually.

You are going to:

1. Setup a microSD card with a bootable Manjaro ARM image
2. Manually setup the NVMe disk with LUKS encryption
3. Manually install Manjaro ARM (Plasma flavor) on the same encrypted NVMe disk
4. Manually setup user(s) accounts
5. Update firmwares

## Burn the Manjaro ARM PBPro Image onto a microSD card ##

You can edit the variables if you want to try out a different OS. I've tried them all, and Manjaro was the best option.

### Defining Variables for Installation ###

All you should really need to change is the `$mSD_Card1`, `$PBP_Images`, and the `$Manjaro_KDE_Version` variables.

Go [here](https://github.com/manjaro-arm/pbpro-images/releases/) to find the most up-to-date version of the Manjaro ARM (PBPro specific) image.

Plug in your microSD card into your PC.

```bash
lsblk
ls -l /dev/disk/by-id/usb*
mSD_Card1="/dev/disk/by-id/usb-Generic-_USB3.0_CRW_-SD_201506301013-0:1"
PBP_Images="/hdd1/PinebookPro_Images"
Manjaro_KDE_Version="22.02"
# You don't have to edit the rest, it's up to you...
Manjaro_KDE_FileName="Manjaro-ARM-kde-plasma-pbpro-$Manjaro_KDE_Version.img.xz"
Manjaro_KDE_Directory="$PBP_Images/Manjaro"
Manjaro_KDE_FilePath="$Manjaro_KDE_Directory/$Manjaro_KDE_FileName"
GitHub_KDE_Download="https://github.com/manjaro-arm/pbpro-images/releases/download/$Manjaro_KDE_Version/$Manjaro_KDE_FileName"
GitHub_KDE_Download_SHA1="$GitHub_KDE_Download.sha1"
GitHub_KDE_SHA1_FilePath="$Manjaro_KDE_FilePath.sha1"
```

### Download the Manjaro ARM (PBPro Specific) Image ###

```bash
mkdir --parents $Manjaro_KDE_Directory
wget --no-clobber --output-document $Manjaro_KDE_FilePath $GitHub_KDE_Download
```

You should check the downloaded XZ image checksum before dd'ing

Make sure the output for the downloaded XZ image is `OK`, and not `FAILED`.

If it's FAILED, then either the .img.xz file is corrupt. Or the sha1 checksum file is invalid.

```bash
pushd $Manjaro_KDE_Directory
wget --no-clobber --output-document $GitHub_KDE_SHA1_FilePath $GitHub_KDE_Download_SHA1
sha1sum --check --ignore-missing $GitHub_KDE_SHA1_FilePath
popd
# This is a way to pipe the SHA1 checksum directly from the internet
# wget -O - "$GitHub_KDE_Download_SHA1" 2> /dev/null | sha1sum --check --ignore-missing
```

### Wipe microSD card ###

This will unmount and wipe the microSD card, __BE CAREFUL!__

```bash
sudo umount --quiet $mSD_Card1*
sudo wipefs --all --quiet $mSD_Card1*
```

### Decompress Image onto microSD Card ###

This will decompress the image to stdout, then the image is piped into dd

No need to decompress the image before using dd!

```bash
xz --stdout --keep --decompress < $Manjaro_KDE_FilePath - | sudo dd status=progress bs=1MiB conv=fsync iflag=fullblock of=$mSD_Card1
```

<!---

This was commented out because you probably will never need this. But it's handy to keep around.

For XZ, but in the other direction (SD Card to file)

```bash
xz --stdout $mSD_Card1 > BackupImage.xz
```

For GZip (You might need to be root) `sudo -i -u root`

```bash
gzip --decompress --stdout DownloadedImage.gz | sudo dd status=progress bs=1M conv=fsync iflag=fullblock of=$mSD_Card1
# I found that you can also skip using dd with gzip. However, it's slower. And needs root.
gzip --decompress --stdout DownloadedImage.gz > $mSD_Card1

```

For GZip, but in the other direction

```bash
gzip --stdout $mSD_Card1 > BackupImage.gz
```
--->

## Unmount and Move the microSD Card to the PBPro ##

You will want to umount the microSD card before removing from your PC.

```bash
sudo umount --quiet $mSD_Card1*
```

Since we are going an encrypted root filesystem, we'll need to customize the installation. DO NOT USE THE INSTALL WIZARD!

- Remove microSD card from your PC after unmounting
- Once you have the microSD card in your grubby hands, install it (pin side down) on the right hand side of the Pinebook Pro
- Power on the Pinebook Pro
- Do __NOT__ use the installation wizard, instead bail to a spare TTY
- __Close__ the installation wizard
- Proceed with the installation from an SSH session (password is: `oem`)
- Open the `Terminal` application, and get the IP address if you can't check the DHCP lease

```bash
ip address show
```

### Continue from SSH ###

From your PC, ssh to the Pinebook Pro.

The default password is: `oem`

```bash
ssh oem@192.168.1.15
```

# Manually Setup Encryption on the NVMe disk #

This part of the tutorial was ripped off from:

<https://web.archive.org/web/20220330012108/https://yahe.sh/pinebook-pro-how-to-install-manjaro-on-a-luks-encrypted-nvme-ssd/>

### Change pacman's Mirrorlist ###

The default mirrors for Manjaro were really slow.

```bash
sudo pacman-mirrors --geoip --api --protocol https
```

### Install Needed Packages ###

```bash
sudo pacman --noconfirm --sync --refresh --refresh bash wget git systemd libarchive openssl gawk dosfstools polkit haveged nvme-cli screen bash-completion rsync pigz htop iotop

sudo systemctl enable --now haveged.service
```

### Use Screen ###

To detach: `Ctrl+a` let go, then you hit `d`

To reattach: `screen -r -D name`

```bash
screen -S pbpro
Ctrl+a
d
screen -r -D pbpro
```

## Define Installation Variables ##

You can find the full list of timezones at `/usr/share/zoneinfo/`

`$EMMC_DEVICE` and `$NVME_NAME` probably changed... Make sure the names are the same with `lsblk`

```bash
lsblk

USERNAME="pbpro"
FULLNAME="Administrator"
USERGROUPS="wheel,sys,audio,input,video,storage,lp,network,users,power"

HOSTNAME="pbpro"

LOCALE="en_US.UTF-8"
TIMEZONE="America/Chicago"
CLIKEYMAP="us"
X11KEYMAP="us"

ARCH="aarch64"
DEVICE="pbpro"
EDITION="kde-plasma"

EMMC_DEVICE="mmcblk2"
EMMC_PARTITION_DEVICE="${EMMC_DEVICE}p1"
EMMC_PARTITION_LABEL="MJRO_BOOT"
EMMC_PARTITION_START_SIZE="32"
EMMC_PARTITION_START="${EMMC_PARTITION_START_SIZE}MiB"
EMMC_PARTITION_END="1024MiB"

NVME_NAME="nvme0"
NVME_DEVICE="${NVME_NAME}n1"
NVME_PARTITION_DEVICE="${NVME_DEVICE}p1"
NVME_VOLUME_LABEL="MJRO_ROOT"
NVME_PARTITION_NAME="${NVME_VOLUME_LABEL}"
NVME_VOLUMEGROUP_NAME="${NVME_VOLUME_LABEL}_lvm"
ROOT_VOLUME_NAME="${NVME_VOLUMEGROUP_NAME}_root"
ROOT_VOLUME_SIZE="100%FREE"

LUKS_ITER_TIME="1637"

SWAP_VOLUME_LABEL="SWAP"
SWAP_VOLUME_NAME="${NVME_VOLUMEGROUP_NAME}_swap"
SWAP_VOLUME_SIZE="12GiB"

NSPAWN="systemd-nspawn --quiet --resolv-conf=copy-host --timezone=off -D"
```

### Setup the Manjaro ARM Profile Variables ###

The manjaro-arm-installer uses so-called "profiles" to decide which packages and services need to be installed for a specific ARM-based platform.

If you want to install Manjaro on a different $DEVICE or use a different Manjaro $EDITION you have to look what the corresponding Git repository has to offer.

Based on the selected `$DEVICE` and `$EDITION` variables we have a corresponding lists of packages to install and services to enable. These lists will be used later on.

```bash
git clone https://gitlab.manjaro.org/manjaro-arm/applications/arm-profiles.git /tmp/arm-profiles/

PKG_DEVICE=$(grep "^[^#;]" "/tmp/arm-profiles/devices/${DEVICE}" | awk '{print $1}')
PKG_EDITION=$(grep "^[^#;]" "/tmp/arm-profiles/editions/${EDITION}" | awk '{print $1}')
SRV_EDITION=$(grep "^[^#;]" "/tmp/arm-profiles/services/${EDITION}" | awk '{print $1}')
```

#### Check the Profile Variables ####

It's wise to check the variables to make sure they work just fine before using them later.

```bash
echo $PKG_DEVICE
echo $PKG_EDITION
echo $SRV_EDITION
```

## Unmount All Partitions ##

The upcoming installation steps will destroy all data stored on the disks. But before that we have to unmount the devices.

```bash
sudo umount --quiet /dev/$EMMC_DEVICE*
sudo umount --quiet /dev/$NVME_DEVICE*
```

## Clear the Bootloader Section off of the eMMC Disk ##

The eMMC disk contains a specific bootloader section. It will be rewritten later on.

```bash
sudo dd if=/dev/zero of=/dev/$EMMC_DEVICE bs=1MiB count=$EMMC_PARTITION_START_SIZE
```

## Create the Boot Partition on the eMMC Disk ##

Now we will create the boot partition. It will be offset by 32MiB to leave room for the bootloader section. We won't put anything more on the eMMC card right now. If you don't want to waste the space, then you are free to add another data partition yourself.

This will wipe the data off of the eMMC disk. BACKUP YOUR DATA BEFORE DOING THIS!

```bash
sudo wipefs --all --quiet /dev/$EMMC_DEVICE*
sudo sgdisk --zap-all /dev/$EMMC_DEVICE
sudo sgdisk --new=1:+$EMMC_PARTITION_START:+$EMMC_PARTITION_END --typecode=0:EF00 --verify /dev/$EMMC_DEVICE
sudo mkfs.fat -F 32 -c -n $EMMC_PARTITION_LABEL /dev/$EMMC_PARTITION_DEVICE
```

## Create the LUKS2 Partition on the NVMe Disk ##

This will wipe the data off of the NVMe disk. BACKUP YOUR DATA BEFORE DOING THIS!

```bash
sudo wipefs --all --quiet /dev/$NVME_DEVICE*
sudo sgdisk --zap-all /dev/$NVME_DEVICE
sudo sgdisk --new=1:0:0 --typecode=1:8300 --verify /dev/$NVME_DEVICE
sudo cryptsetup --batch-mode --use-urandom --iter-time $LUKS_ITER_TIME luksFormat /dev/$NVME_PARTITION_DEVICE
```

## Open the LUKS2 Partition from the NVMe Disk ##

To proceed, we need to open the LUKS-encrypted partition that we just created.

```bash
sudo cryptsetup --verbose open /dev/$NVME_PARTITION_DEVICE $NVME_VOLUME_LABEL
```

## Create the LVM volumes within the LUKS partition ##

Within the LUKS partition we create LVM volumes. As I am lazy, we will only create a swap partition and a root partition for everything else.

Feel free to introduce a more sophisticated partition layout if you feel like it.

```bash
sudo pvcreate /dev/mapper/$NVME_PARTITION_NAME
sudo vgcreate $NVME_VOLUMEGROUP_NAME /dev/mapper/$NVME_PARTITION_NAME
sudo lvcreate -L $SWAP_VOLUME_SIZE $NVME_VOLUMEGROUP_NAME -n $SWAP_VOLUME_NAME
sudo lvcreate -l $ROOT_VOLUME_SIZE $NVME_VOLUMEGROUP_NAME -n $ROOT_VOLUME_NAME
sudo mkswap /dev/$NVME_VOLUMEGROUP_NAME/$SWAP_VOLUME_NAME
sudo swaplabel -L $SWAP_VOLUME_LABEL /dev/$NVME_VOLUMEGROUP_NAME/$SWAP_VOLUME_NAME
sudo mkfs.ext4 -O metadata_csum,64bit /dev/$NVME_VOLUMEGROUP_NAME/$ROOT_VOLUME_NAME
```

### Change the LUKS Passphrase (Optional) ###

Only do this if you want to change the passphrase on the LUKS encrypted NVMe partition

First, find out how many key slots are in use.

```bash
sudo cryptsetup luksDump /dev/$NVME_PARTITION_DEVICE
```

There should only be one key slot in use: `0`

This is another way to easily count how many key slots are in use

```bash
sudo cryptsetup luksDump /dev/$NVME_PARTITION_DEVICE | grep luks2 | uniq | wc -l
```

If the answer was __1__, then that means `--key-slot 0` is the one any only key slot.

If the answer was __3__, then that means `--key-slot 0`, `--key-slot 1`, `--key-slot 2` are all in use.

```bash
sudo cryptsetup --key-slot 0 --iter-time $LUKS_ITER_TIME luksChangeKey /dev/$NVME_PARTITION_DEVICE
```

# Manually Install Manjaro ARM (Plasma flavor) on the Newly Encrypted NVMe Disk #

## Mount the Root and Boot Partitions ##

Mount the newly created partitions to `/mnt` and to `/mnt/boot`

```bash
sudo mount /dev/$NVME_VOLUMEGROUP_NAME/$ROOT_VOLUME_NAME /mnt
sudo mkdir /mnt/boot
sudo mount /dev/$EMMC_PARTITION_DEVICE /mnt/boot
```

## Install the Base Image onto the Encypted NVMe Disk ##

```bash
wget --no-clobber --output-document /tmp/Manjaro-ARM-$ARCH-latest.tar.gz "https://github.com/manjaro-arm/rootfs/releases/latest/download/Manjaro-ARM-$ARCH-latest.tar.gz"
sudo pigz --decompress --keep --stdout /tmp/Manjaro-ARM-$ARCH-latest.tar.gz | sudo tar --extract --file - --directory /mnt
```

## Prepare the Package Manager ##

Before we can install additional packages, we need to prepare the package manager `pacman`.

This is the first time we will use the command stored in `$NSPAWN`, which will execute commands in the context of our newly installed operating system.

```bash
sudo $NSPAWN /mnt pacman-key --init

sudo $NSPAWN /mnt pacman-key --populate archlinux archlinuxarm manjaro manjaro-arm
```

## Install the $DEVICE and $EDITION Specific Packages ##

After preparing the package manager, we can install additional packages into our new system.

There are some basic packages, as well as the `$DEVICE` specific, and the `$EDITION` specific packages.

Feel free to add other packages that are relevant to you. This process will take a while.

```bash
sudo $NSPAWN /mnt pacman-mirrors --geoip --api --protocol https

sudo $NSPAWN /mnt pacman --sync --sysupgrade --refresh --refresh base manjaro-system manjaro-release systemd systemd-libs haveged nvme-cli screen bash-completion rsync pigz htop iotop $PKG_EDITION $PKG_DEVICE
```

### Choosing Default Software ###

I went with the following default software.

#### jack ####

1. jack2
2. __pipewire-jack__

#### pipewire-session-manager ####

1. pipewire-media-session
2. __wireplumber__

#### phonon-qt5-backend ####

1. __phonon-qt5-gstreamer__
2. phonon-qt5-vlc

#### libmm-glib.so=0-64 ####

1. __libmm-glib__
2. libmm-pp-glib

#### initramfs ####

1. __mkinitcpio__
2. booster
3. dracut


## Enable the $SERVICES ##

There are some services that got installed and that need to be enabled so that they start automatically.

Should you have installed additional services then don't forget to enable them as well.

```bash
sudo $NSPAWN /mnt systemctl enable getty.target haveged.service
```

The services `bootsplash-hide-when-booted.service` and `bootsplash-show-on-shutdown.service` weren't present at install.

So I ended up ripping those services out of `$SRV_EDITION`.

```bash
SRV_EDITION=$(grep "^[^#;]" "/tmp/arm-profiles/services/${EDITION}" | grep --invert-match "bootsplash-hide-when-booted.service\|bootsplash-show-on-shutdown.service" | awk '{print $1}')
sudo $NSPAWN /mnt systemctl enable $SRV_EDITION

if [ -f /mnt/usr/bin/xdg-user-dirs-update ]; then sudo $NSPAWN /mnt systemctl --global enable xdg-user-dirs-update.service ; fi
```

### Disable the zswap Service ###

Thanks to our LVM setup, we have a real swap partition and don't have to rely on zswap.

```bash
sudo $NSPAWN /mnt systemctl disable zswap-arm.service
```

## Apply the Overlay for the Selected $EDITION ##

The overlays are basically just a bunch of files that will overwrite default files with `$EDITION` specific versions.

Therefore, we just copy them to the NVMe disk.

```bash
sudo rsync --recursive --progress --verbose --verbose --times --hard-links --numeric-ids --checksum --compress --compress-choice=zstd --compress-level=10 --checksum-choice=xxh3 /tmp/arm-profiles/overlays/$EDITION/ /mnt/
```

# Manually Creating User(s) #

## Adding Users with sudo Privileges ##

We will only create one user in addition to the root user for now.

We'll test the password too by dropping to the $USERNAME account.

```bash
sudo $NSPAWN /mnt useradd --create-home --groups $USERGROUPS --shell /bin/bash $USERNAME

sudo $NSPAWN /mnt chfn --full-name "$FULLNAME" $USERNAME

sudo $NSPAWN /mnt passwd $USERNAME

sudo sed --in-place "s/# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/g" /mnt/etc/sudoers
```

### Removing Root Login Access ###

```bash
sudo mkdir /mnt/etc/ssh/sshd_config.d/
echo "PermitRootLogin no" | sudo tee /mnt/etc/ssh/sshd_config.d/no_root.conf

sudo $NSPAWN /mnt usermod --lock root

sudo $NSPAWN /mnt usermod --expire 1970-01-01 root
```

Consider just adding a strong and unique password to the root account

```bash
sudo $NSPAWN /mnt passwd root
```

## Enable the User's Services ##

PipeWire is a user-specific service. So we can enable it now.

You must create the user first.

```bash
if [[ "$EDITION" != "minimal" ]] && [[ "$EDITION" != "server" ]]; then sudo $NSPAWN /mnt --user $USERNAME systemctl --user enable pipewire-pulse.service ; fi
```

## Setup the Newly Installed OS ##

Now it is time to setup the system, including keyboard layouts, timezones, etc.

```bash
sudo $NSPAWN /mnt ln --symbolic --force /usr/share/zoneinfo/$TIMEZONE /etc/localtime

sudo $NSPAWN /mnt ls -l /etc/localtime

sudo sed --in-place "s/"#$LOCALE"/$LOCALE/g" /mnt/etc/locale.gen

for CHANGED_LOCALES in LANG LANGUAGE LC_ADDRESS LC_COLLATE LC_CTYPE LC_IDENTIFICATION LC_MEASUREMENT LC_MESSAGES LC_MONETARY LC_NAME LC_NUMERIC LC_PAPER LC_TELEPHONE LC_TIME ; do echo "$CHANGED_LOCALES=$LOCALE" | sudo tee --append /mnt/etc/locale.conf ; done

sudo $NSPAWN /mnt locale-gen

echo "KEYMAP=$CLIKEYMAP" | sudo tee /mnt/etc/vconsole.conf

if [[ "$EDITION" != "minimal" ]]; then
cat << EOF | sudo tee /mnt/etc/X11/xorg.conf.d/00-keyboard.conf
Section "InputClass"
Identifier "system-keyboard"
Option "XkbLayout" "$X11KEYMAP"
EndSection
EOF
fi

if [[ "$EDITION" = "sway" ]]; then sed --in-place "s/us/$X11KEYMAP"/ /mnt/etc/sway/inputs/default-keyboard ; fi

echo "$HOSTNAME" | sudo tee /mnt/etc/hostname

echo -e "\n127.0.1.1       $HOSTNAME\n" | sudo tee --append /mnt/etc/hosts

sudo sed --in-place "s/enable systemd-resolved.service/#enable systemd-resolved.service/" /mnt/usr/lib/systemd/system-preset/90-systemd.preset

if [[ "$EDITION" = "cubocore" ]]; then sudo cp /mnt/usr/share/applications/corestuff.desktop /mnt/etc/xdg/autostart/ ; fi
```

## Tweak initrd ##

Let's prepare the initial ramdisk for our device.

I looked at both of the existing conf files to make sure there weren't new kernel modules to add. The tutorial I ripped this off from had used some outdated modules.

```bash
sudo cat /mnt/etc/mkinitcpio.conf
sudo cat /etc/mkinitcpio.conf

cat << EOF | sudo tee /mnt/etc/mkinitcpio.conf
MODULES=(rtc_rk808 rockchipdrm panel_edp pwm_bl panfrost drm_kms_helper hantro_vpu analogix_dp rockchip_rga arc_uart cw2015_battery i2c-hid iscsi_boot_sysfs jsm uhid)
BINARIES=()
FILES=()
HOOKS=(base udev keyboard plymouth autodetect keymap modconf block plymouth-encrypt lvm2 filesystems fsck )
COMPRESSION="cat"
EOF

sudo $NSPAWN /mnt mkinitcpio --preset linux
```

## Add the Root and Boot Partitions to /etc/fstab ##

Let's create the fstab file so that our partitions can be mounted automatically.

```bash
BOOT_UUID=$(sudo blkid -s UUID -o value "/dev/$EMMC_PARTITION_DEVICE")
ROOT_UUID=$(sudo blkid -s UUID -o value "/dev/$NVME_VOLUMEGROUP_NAME/$ROOT_VOLUME_NAME")
SWAP_UUID=$(sudo blkid -s UUID -o value "/dev/$NVME_VOLUMEGROUP_NAME/$SWAP_VOLUME_NAME")

cat << EOF | sudo tee /mnt/etc/fstab
# <file system>                            <dir>   <type>  <options>                       <dump>  <pass>
UUID=$BOOT_UUID                             /boot   vfat    defaults,noexec,nodev,showexec  0       0
UUID=$ROOT_UUID  /       ext4    defaults                        0       1
UUID=$SWAP_UUID  none    swap    sw                              0       0
EOF
```

## Add the Partitions to crypttab ##

The same is necessary for the crypttab file so that our encrypted partitions are mounted automatically as well.

```bash
NVME_UUID=$(sudo blkid -s UUID -o value "/dev/$NVME_PARTITION_DEVICE")

cat << EOF | sudo tee /mnt/etc/crypttab
$NVME_PARTITION_NAME UUID=$NVME_UUID    none    luks,discard,x-initrd.attach
EOF
```

## Flash the Bootloader ##

Now it is finally time to write the bootloader to the specific bootloader section.

```bash
sudo dd if=/mnt/boot/idbloader.img of=/dev/$EMMC_DEVICE seek=64 conv=notrunc
sudo dd if=/mnt/boot/u-boot.itb of=/dev/$EMMC_DEVICE seek=16384 conv=notrunc
```

## Update the Boot Configuration ##

We finalize the boot configuration now by setting the correct boot parameters.

This way the boot process will know where to find the root partition and it will know that it is encrypted.

```bash
head --lines -1 /mnt/boot/extlinux/extlinux.conf | sudo tee /mnt/boot/extlinux/extlinux.conf.new
sudo mv /mnt/boot/extlinux/extlinux.conf /mnt/boot/extlinux/extlinux.conf.bak
sudo mv /mnt/boot/extlinux/extlinux.conf.new /mnt/boot/extlinux/extlinux.conf

echo "APPEND initrd=/initramfs-linux.img console=ttyS2,1500000 cryptdevice=UUID=$NVME_UUID:$NVME_PARTITION_NAME root=UUID=$ROOT_UUID rw rootwait audit=0 splash plymouth.ignore-serial-consoles video=eDP-1:1920x1080@60 video=HDMI-A-1:1920x1080@60" | sudo tee --append /mnt/boot/extlinux/extlinux.conf
cat /mnt/boot/extlinux/extlinux.conf
```

## Clean up Files ##

At first we get rid of temporary files.

I'm not sure if the machine-id file was worth trashing. I left it in this write up.

```bash
sudo $NSPAWN /mnt pacman --noconfirm --sync --clean

sudo rm --recursive /mnt/var/log
sudo rm /mnt/etc/machine-id
```

## Unmount the Partitions ##

Unmount the newly written partitions.

```bash
sudo umount /mnt/boot
sudo umount /mnt*

sudo vgchange --activate n

sudo cryptsetup close "/dev/mapper/$NVME_PARTITION_NAME"

sudo partprobe "/dev/$EMMC_DEVICE"
sudo partprobe "/dev/$NVME_DEVICE"
```

## Shutdown ##

If you have come to this point then you have done it. You have installed Manjaro by-hand on a PineBook Pro, including an encrypted root partition that is stored on the separate NVMe SSD.

Currently, you are running Manjaro from an microSD card which needs to be removed before booting from the internal eMMC card.

So shut the device done, remove the microSD card and then boot normally.

During the boot process you should be asked for your LUKS password and then the boot process should continue until it reaches the login screen.

```bash
sudo poweroff
```

## Remove microSD card ##

Remove the microSD card once the Pinebook Pro has powered off...

## Turn Pinebook Pro back on ##

You should be able to login now to Manjaro on the Pinebook Pro...

# Update Firmwares #

## Touchpad Firmware ##

This warning may or may not apply

I updated the firmware and applied the newest firmware

<https://github.com/dragan-simic/pinebook-pro-keyboard-updater/>

> Warning: DO NOT update the touchpad firmware before checking which keyboard IC your Pinebook Pro has. Some Pinebook Pro were delivered with a SH61F83 instead of a SH68F83. The SH61F83 can only be written 8 times, this will render the keyboard and touchpad unusable if this limit is reached when step-1 is flashed. See Reddit SH61F83 thread. The keyboard IC corresponds to U23 on the top layer silkscreen of the main board. It is located under the keyboard flat flexible cable.

(No optional dependancies)

```bash
sudo pamac install pinebook-pro-keyboard-updater base-devel libusb vim
```

#### Careful ####

This will __stop the trackpad AND keyboard until you are done with step 2.__

Put power on mains.

Use ssh or an external keyboard.

```bash
sudo pbp-fwupdate step-1
sudo poweroff
```

Note, your trackpad is __still__ off....

Now you have to update the keyboard

```bash
sudo pbp-fwupdate step-2 ansi
sudo poweroff
```

Turn the Pinebook Pro back on. You should now be okay to prceed.

## Keyboard Firmware ##

I also installed the updated firmware too. March 2020 - default_ansi.hex

```bash
git clone https://github.com/dragan-simic/pinebook-pro-keyboard-updater.git
cd ~/pinebook-pro-keyboard-updater/
sudo pbp-fwupdate flash-kb firmware/default_ansi.hex
sudo poweroff
```

Note, your trackpad is __still__ off....

Now you have to update the keyboard

```bash
sudo pbp-fwupdate step-2 ansi
sudo poweroff
```

Turn the Pinebook Pro back on. That should be the last of the fiwmare updates.
